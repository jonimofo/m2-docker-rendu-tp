# M2 Docker - Rendu
## Contexte du projet
Nous sommes deux (Guillaume et Benjamin) et nous travaillons ensemble dans la même enteprise.  
Dans le cadre de ce projet, nous souhaitions pouvoir utiliser le support de cours pour répondre directement à nos besoins métiers d'entreprise.

Nous avons tous deux un profil IT différent : Guillaume étant plutôt orienté dev web et Benjamin orienté infra/réseau.  
Dans ce sens, après une phase initiale de réflexion collective, nous avons décidé de nous organiser de la façon suivante :
- ```Benjamin```
    - Définitions des besoins en terme d'infra
    - Ecriture des playbooks ansible du Setup Server
    - Mise en place CI/CD des playbooks ansible
- ```Guillaume```
    - Définition des besoins pour l'environnement de dev
    - Développement de l'application PHP
    - Mise en place CI/CD des tests unitaires PHP

## Rendu du TP
Le rendu du TP est organisé en 3 parties :
- Le présent dépôt qui contient le contexte général du projet nos retours d'expériences
- [Le dépôt](https://gitlab.com/jonimofo/m2-docker-ansible) qui contient les playbooks Ansible et les tests Molecule pour la CI/CD
- [Le dépôt](https://gitlab.com/jonimofo/m2-docker-appli-metier-php) qui contient l'application PHP et les tests unitaires pour la CI/CD

## Difficultés rencontrées
- Comprendre et mettre en relation la CI/CD par rapport à notre pratique (notamment le push de notre image dans le Registry)
- Collaboration parfois ralentie entre un profil dev et un profil infra
- Trouver les images Docker adaptées à nos différents déploiements
- Structure du fichier gitlab-ci.yml à creuser

## Retours d'expérience
Dans le cadre de ce projet nous avons pu :
- Acquérir une expérience DevOps en répondant directement à un besoin métier
- Explorer Ansible et Molecule
- Préparer des ressources qui seront directement utilisées dans le cadre de notre travail (playbooks ansible et CI/CD Gitlab)
- Comprendre l'enjeu primordial du DevOps en confrontant l'un l'autre les différents problèmes rencontrés et en tâchant de les résoudre ensemble

## Pour aller plus loin
Pour aller plus loin dans ce projet il serait également pertinent de :
- Monter nous-mêmes notre Gitlab pour ne pas être limité par les runners
- Monter nous-mêmes notre Registry
- Combiner nos pipelines : playbooks qui testent et déploient intégralement l'application PHP

